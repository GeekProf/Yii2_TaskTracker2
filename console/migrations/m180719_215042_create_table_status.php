<?php

use yii\db\Migration;

/**
 * Class m180719_215042_create_table_status
 */
class m180719_215042_create_table_status extends Migration {

    /**
     * {@inheritdoc}
     */
    public function safeUp() {

        $tableOptions = null;

        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('status', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'created_at' => $this->dateTime()->notNull(),
            'updated_at' => $this->dateTime()->notNull()
                ], $tableOptions);


        $this->addForeignKey('fk_task_status', 'task', 'status', 'status', 'id');
        $this->addForeignKey('fk_project_status', 'project', 'status', 'status', 'id');
        
       
          $this->batchInsert('status', ['name', 'created_at', 'updated_at'], [
        ['Create', time(), time()],
        ['Work', time(), time()],
        ['Wait', time(), time()],
        ['Complete', time(), time()],
       
    ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown() {
        echo "m180719_215042_create_table_status cannot be reverted.\n";

        return false;
    }

    /*
      // Use up()/down() to run migration code without a transaction.
      public function up()
      {

      }

      public function down()
      {
      echo "m180719_215042_create_table_status cannot be reverted.\n";

      return false;
      }
     */
}
