<?php

use yii\db\Migration;

/**
 * Class m180607_071422_add_column_to_task
 */
class m180607_071422_add_column_to_task extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('task', 'user_id', $this->integer());
        $this->addForeignKey('fk_task_user', 'task', 'user_id', 'user', 'id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180607_071422_add_column_to_task cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180607_071422_add_column_to_task cannot be reverted.\n";

        return false;
    }
    */
}
