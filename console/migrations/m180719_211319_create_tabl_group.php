<?php

use yii\db\Migration;

/**
 * Class m180719_211319_create_tabl_group
 */
class m180719_211319_create_tabl_group extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
 $tableOptions = null;
 
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('group', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'description' => $this->string()->notNull(),
            'created_at' => $this->dateTime()->notNull(),
            'updated_at' => $this->dateTime()->notNull()
        ], $tableOptions);
        
        $this->addColumn('project', 'group_id', $this->integer());
        $this->addForeignKey('fk_group_project', 'project', 'group_id', 'group', 'id');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180719_211319_create_tabl_group cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180719_211319_create_tabl_group cannot be reverted.\n";

        return false;
    }
    */
}
