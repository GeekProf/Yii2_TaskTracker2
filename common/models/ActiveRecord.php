<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace common\models;

/**
 * Description of ActiveRecord
 *
 * @author USER
 */
class ActiveRecord extends \yii\db\ActiveRecord{
    /*3. Используя поведения обеспечить автоматическое обновления данных 
     * о времени создания и обновления записей в базе  */
    public function behaviors() {

        return [
        [
        'class' => \yii\behaviors\TimestampBehavior::ClassName(),
        'createdAtAttribute' => 'created_at',
        'updatedAtAttribute' => 'updated_at',
        'value' => new \yii\db\Expression('NOW()'),
        ],
        ];
    }
}
