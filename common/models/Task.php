<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "task".
 *
 * @property int $id
 * @property string $name
 * @property string $description
 * @property int $status
 * @property string $due_date
 * @property string $created_at
 * @property string $updated_at
 */
class Task extends ActiveRecord {

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'task';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['name', 'description', 'due_date', 'created_at', 'updated_at'], 'required'],
            [['status'], 'integer'],
            [['due_date', 'created_at', 'updated_at'], 'safe'],
            [['name', 'description'], 'string', 'max' => 255],
        ];
    }

            public static function getTaskByProject($progectid) {
        return static::find()
                        ->where(['project_id' => $progectid])
                        ->all();
    }
    

    
    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'description' => Yii::t('app', 'Description'),
            'status' => Yii::t('app', 'Status'),
            'due_date' => Yii::t('app', 'Due Date'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * {@inheritdoc}
     * @return TaskQuery the active query used by this AR class.
     */
    public static function find() {
        return new TaskQuery(get_called_class());
    }

    public static function getByCurrentMonth($userId) {
        return static::find()
                        ->where(['user_id' => $userId])
                        ->andWhere(['MONTH(due_date)' => date('n')])
                        ->all();
    }

    /*
     * таски с истекающим сроком выполнения, и отправлять оповещение об этом исполнителям.
     */

    public static function getOverdue() {
        return static::find()
                        ->where(['DATE(`due_date`)' => date("Y-m-d")])
                        ->all();
    }

}
