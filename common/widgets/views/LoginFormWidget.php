<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Modal;

Modal::begin([
    'header' => '<h4>'.Yii::t('app', 'Login').'</h4>',
    'id' => 'login-modal',
]);
?>

    <p><?= Yii::t('app','Please fill out the following fields:') ?></p>

<?php
$form = ActiveForm::begin([
            'id' => 'login-form',
            'enableAjaxValidation' => true,
            'action' => ['site/ajax-login']
        ]);
echo $form->field($model, 'username')->textInput();
echo $form->field($model, 'password')->passwordInput();
echo $form->field($model, 'rememberMe')->checkbox();
?>

<div>
    <?=Yii::t('app','If you forgot your password you can');  ?>
<?=Html::a(Yii::t('app','reset it'), ['site/request-password-reset']) ?>.
</div>
<div class="form-group">
    <div class="text-right">

        <?php
        echo Html::button(Yii::t('app', 'Cancel') , ['class' => 'btn btn-default', 'data-dismiss' => 'modal']);
        echo Html::submitButton(Yii::t('app', 'Login'), ['class' => 'btn btn-primary', 'name' => 'login-button']);
        ?>

    </div>
</div>

<?php
ActiveForm::end();
Modal::end();
