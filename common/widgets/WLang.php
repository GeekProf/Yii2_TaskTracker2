<?php
namespace common\widgets;
use common\models\Lang;
use yii\helpers\ArrayHelper;
class WLang extends \yii\bootstrap\Widget
{
    public function init(){}

    public function run() {
        $items = Lang::find()->all();       
        return $this->render('lang\lang_widget', ['items'=>$items
        ]);
    }
}