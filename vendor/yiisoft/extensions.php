<?php

$vendorDir = dirname(__DIR__);

return array (
  'yiisoft/yii2-bootstrap' => 
  array (
    'name' => 'yiisoft/yii2-bootstrap',
    'version' => '2.0.8.0',
    'alias' => 
    array (
      '@yii/bootstrap' => $vendorDir . '/yiisoft/yii2-bootstrap/src',
    ),
  ),
  'yiisoft/yii2-debug' => 
  array (
    'name' => 'yiisoft/yii2-debug',
    'version' => '2.0.13.0',
    'alias' => 
    array (
      '@yii/debug' => $vendorDir . '/yiisoft/yii2-debug',
    ),
  ),
  'yiisoft/yii2-gii' => 
  array (
    'name' => 'yiisoft/yii2-gii',
    'version' => '2.0.7.0',
    'alias' => 
    array (
      '@yii/gii' => $vendorDir . '/yiisoft/yii2-gii/src',
    ),
  ),
  'yiisoft/yii2-faker' => 
  array (
    'name' => 'yiisoft/yii2-faker',
    'version' => '2.0.4.0',
    'alias' => 
    array (
      '@yii/faker' => $vendorDir . '/yiisoft/yii2-faker',
    ),
  ),
  'rmrevin/yii2-fontawesome' => 
  array (
    'name' => 'rmrevin/yii2-fontawesome',
    'version' => '2.17.1.0',
    'alias' => 
    array (
      '@rmrevin/yii/fontawesome' => $vendorDir . '/rmrevin/yii2-fontawesome',
    ),
  ),
  'cebe/yii2-gravatar' => 
  array (
    'name' => 'cebe/yii2-gravatar',
    'version' => '1.1.0.0',
    'alias' => 
    array (
      '@cebe/gravatar' => $vendorDir . '/cebe/yii2-gravatar/cebe/gravatar',
    ),
  ),
  'hiqdev/yii2-collection' => 
  array (
    'name' => 'hiqdev/yii2-collection',
    'version' => '0.1.1.0',
    'alias' => 
    array (
      '@hiqdev/yii2/collection' => $vendorDir . '/hiqdev/yii2-collection/src',
    ),
  ),
  'hiqdev/yii2-thememanager' => 
  array (
    'name' => 'hiqdev/yii2-thememanager',
    'version' => '0.3.2.0',
    'alias' => 
    array (
      '@hiqdev/thememanager' => $vendorDir . '/hiqdev/yii2-thememanager/src',
    ),
  ),
  'hiqdev/yii2-menus' => 
  array (
    'name' => 'hiqdev/yii2-menus',
    'version' => '0.1.0.0',
    'alias' => 
    array (
      '@hiqdev/yii2/menus' => $vendorDir . '/hiqdev/yii2-menus/src',
    ),
  ),
  'hiqdev/yii2-theme-agency' => 
  array (
    'name' => 'hiqdev/yii2-theme-agency',
    'version' => '0.1.0.0',
    'alias' => 
    array (
      '@hiqdev/themes/agency' => $vendorDir . '/hiqdev/yii2-theme-agency/src',
    ),
  ),
  'yiisoft/yii2-jui' => 
  array (
    'name' => 'yiisoft/yii2-jui',
    'version' => '2.0.7.0',
    'alias' => 
    array (
      '@yii/jui' => $vendorDir . '/yiisoft/yii2-jui',
    ),
  ),
  'yii2mod/yii2-enum' => 
  array (
    'name' => 'yii2mod/yii2-enum',
    'version' => '1.7.1.0',
    'alias' => 
    array (
      '@yii2mod/enum' => $vendorDir . '/yii2mod/yii2-enum',
    ),
  ),
  'yii2mod/yii2-moderation' => 
  array (
    'name' => 'yii2mod/yii2-moderation',
    'version' => '1.2.0.0',
    'alias' => 
    array (
      '@yii2mod/moderation' => $vendorDir . '/yii2mod/yii2-moderation',
    ),
  ),
  'yii2mod/yii2-editable' => 
  array (
    'name' => 'yii2mod/yii2-editable',
    'version' => '1.5.0.0',
    'alias' => 
    array (
      '@yii2mod/editable' => $vendorDir . '/yii2mod/yii2-editable',
    ),
  ),
  'yii2mod/yii2-behaviors' => 
  array (
    'name' => 'yii2mod/yii2-behaviors',
    'version' => '1.2.0.0',
    'alias' => 
    array (
      '@yii2mod/behaviors' => $vendorDir . '/yii2mod/yii2-behaviors',
    ),
  ),
  'paulzi/yii2-sortable' => 
  array (
    'name' => 'paulzi/yii2-sortable',
    'version' => '1.0.2.0',
    'alias' => 
    array (
      '@paulzi/sortable' => $vendorDir . '/paulzi/yii2-sortable',
    ),
  ),
  'paulzi/yii2-adjacency-list' => 
  array (
    'name' => 'paulzi/yii2-adjacency-list',
    'version' => '2.1.0.0',
    'alias' => 
    array (
      '@paulzi/adjacencyList' => $vendorDir . '/paulzi/yii2-adjacency-list',
    ),
  ),
  'asofter/yii2-imperavi-redactor' => 
  array (
    'name' => 'asofter/yii2-imperavi-redactor',
    'version' => '0.0.3.0',
    'alias' => 
    array (
      '@yii/imperavi' => $vendorDir . '/asofter/yii2-imperavi-redactor/yii/imperavi',
    ),
  ),
  'yii2mod/yii2-comments' => 
  array (
    'name' => 'yii2mod/yii2-comments',
    'version' => '1.9.9.2',
    'alias' => 
    array (
      '@yii2mod/comments' => $vendorDir . '/yii2mod/yii2-comments',
    ),
  ),
  'yiister/yii2-gentelella' => 
  array (
    'name' => 'yiister/yii2-gentelella',
    'version' => '1.3.1.0',
    'alias' => 
    array (
      '@yiister/gentelella' => $vendorDir . '/yiister/yii2-gentelella',
    ),
  ),
  'yiisoft/yii2-swiftmailer' => 
  array (
    'name' => 'yiisoft/yii2-swiftmailer',
    'version' => '2.1.1.0',
    'alias' => 
    array (
      '@yii/swiftmailer' => $vendorDir . '/yiisoft/yii2-swiftmailer/src',
    ),
  ),
  'dmstr/yii2-adminlte-asset' => 
  array (
    'name' => 'dmstr/yii2-adminlte-asset',
    'version' => '2.6.2.0',
    'alias' => 
    array (
      '@dmstr' => $vendorDir . '/dmstr/yii2-adminlte-asset',
    ),
  ),
  'codemix/yii2-localeurls' => 
  array (
    'name' => 'codemix/yii2-localeurls',
    'version' => '1.7.1.0',
    'alias' => 
    array (
      '@codemix/localeurls' => $vendorDir . '/codemix/yii2-localeurls',
    ),
  ),
  'purrweb/yii2-heroku' => 
  array (
    'name' => 'purrweb/yii2-heroku',
    'version' => '1.0.0.0',
    'alias' => 
    array (
      '@purrweb/heroku' => $vendorDir . '/purrweb/yii2-heroku',
    ),
  ),
);
