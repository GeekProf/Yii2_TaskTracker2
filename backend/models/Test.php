<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\models;

use \yii\base\Model;

/**
 * Description of Test
 *
 * @author USER
 */
class Test extends Model {

    public $title;
    public $content;

    public function rules() {
        return
                [[['title', 'content'], 'required']];
    }

}
