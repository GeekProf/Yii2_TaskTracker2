<?php

namespace app\models;
 
use yii\rbac\Rule;
 
class OwnerRule extends Rule
{
    public $name = 'isOwner';
 
    /**
     * Проверка авторства статьи.
     *
     * @param string|int $user   Идентификатор пользователя.
     * @param Item       $item   Роль или разрешение ассоциированное с этим правилом.
     * @param array      $params Параметры.
     *
     * @return bool Результат проверки, вернет true или false.
     */
    public function execute($user, $item, $params)
    {
        if (!isset($params['article'])) {
            return false;
        }
 
        return $params['article']->user_id == $user;
    }
}