<?php
/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">
    <div class="row">   
        <div class="col-xs-12 col-md-3">
            <?=
            \yiister\gentelella\widgets\StatsTile::widget(
                    [
                        'icon' => 'users',
                        'header' => Yii::t('app', 'Users'),
                        'text' => Yii::t('app', 'Count of registered users'),
                        'number' => $countUser,
                    ]
            )
            ?>
        </div>
        <div class="col-xs-12 col-md-3">
            <?=
            \yiister\gentelella\widgets\StatsTile::widget(
                    [
                        'icon' => 'list-alt',
                        'header' => Yii::t('app', 'Groups'),
                        'text' => Yii::t('app', 'All Groups list'),
                        'number' => $countGroup,
                    ]
            )
            ?>
        </div>
        <div class="col-xs-12 col-md-3">
            <?=
            \yiister\gentelella\widgets\StatsTile::widget(
                    [
                        'icon' => 'cogs',
                        'header' => Yii::t('app', 'Projects'),
                        'text' => Yii::t('app', 'Open Projects'),
                        'number' => $countProject,
                    ]
            )
            ?>
        </div>

        <div class="col-xs-12 col-md-3">
            <?=
            \yiister\gentelella\widgets\StatsTile::widget(
                    [
                        'icon' => 'leaf',
                        'header' => Yii::t('app', 'Tasks'),
                        'text' => Yii::t('app', 'All activ tasks'),
                        'number' => $countTask,
                    ]
            )
            ?>
        </div>
    </div>
    <?php
    if (Yii::$app->user->isGuest) {

     echo   " <div class=\"jumbotron\">
             <h1>". Yii::t('app','Congratulations!')."</h1>
             <p class=\"lead\">".Yii::t('app','You had the privilege of being a part of our nigger team')."</p>
             <p><a class=\"btn btn-lg btn-success\" href=\"site/signup\">".Yii::t('app','Become a Niger of the day')."</a></p>
             </div>";
    }
    ?>
    <div class="body-content">

        <div class="row">
            <div class="col-md-2">
                <div class="btn-group" role="group" aria-label="Basic example">
                    <p><a type="button" class="btn btn-default" href="http://www.yiiframework.com/doc/">Yii <?= Yii::t('app', 'Documentation'); ?></a></p>
                    <p><a type="button" class="btn btn-default" href="http://www.yiiframework.com/extensions/">Yii <?= Yii::t('app', 'Extensions'); ?></a></p>
                    <p><a type="button" class="btn btn-default" href="http://www.yiiframework.com/forum/">Yii <?= Yii::t('app', 'Forum'); ?></a></p>
                </div>
            </div>
        </div>

    </div>
</div>
