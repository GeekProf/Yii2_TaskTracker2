<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use \yiister\gentelella\widgets\Panel;

echo '<div class="row">';
echo '<div  class="col-xs-12 col-md-6 col-lg-4">';
echo Html::a(Yii::t('app', 'Create Project'), ['/project/create'], ['class' => 'btn btn-primary']);
echo '</div>';
echo '</div>';
echo '<div class="row">';

foreach ($items as $project):

    echo '<div  class="col-xs-12 col-md-6 col-lg-4">';

    Panel::begin(
            [
                'header' => $project->name,
                   'icon' => 'cogs'
            ]
    );

    echo '<p>' . $project->description . '</p>';

    echo Html::a(Yii::t('app', 'Project Task'), ['task/project-task?projectid='.$project->id], ['class' => 'btn btn-success pull-right']);
    echo Html::a(Yii::t('app', 'Create Task'), ['/task/create?projectid='.$project->id], ['class' => 'btn btn-warning pull-right']);
    Panel::end();

    echo '</div>';


endforeach;
echo '</div>';
?>  
