<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use \yiister\gentelella\widgets\Panel;

echo '<div class="row">';
echo '<div  class="col-xs-12 col-md-6 col-lg-4">';
echo Html::a(Yii::t('app', 'Create Task'), ['/task/create'], ['class' => 'btn btn-primary']);
echo '</div>';
echo '</div>';
echo '<div class="row">';

foreach ($items as $task):

    echo '<div  class="col-xs-12 col-md-8 col-lg-8">';

    Panel::begin(
            [
                'header' => $task->name ,
                   'icon' => 'leaf',
                   'headerMenu' => [
                    [
                        'label' => 'The first item',
                        'url' => '#',
                    ],
                    [
                        'label' => 'The second item',
                        'url' => '#',
                    ],
                ]
                ]
    );

  
echo '<h5>'.Yii::t('app', 'Status').' - '.$task->status  . '</h5>';
        echo '<p>'.Yii::t('app', 'Description :') . $task->description . '</p>';
    echo Html::a(Yii::t('app', 'Work it'), ['/task/workit?task='.$task->id], ['class' => 'btn btn-warning pull-right']);
    Panel::end();

    echo '</div>';


endforeach;
echo '</div>';
?>  
