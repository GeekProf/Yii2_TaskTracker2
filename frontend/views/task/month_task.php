<?php

/** @var array $tasks */
use yii\helpers\Html;
use yii\helpers\Url;
?>
<p>
<?= Html::a(Yii::t('app', 'Create New Task'), ['create-new'], ['class' => 'btn btn-success']) ?>
</p>

<table class="table table-bordered table-striped table-responsive">
    <tr>
        <td><?= Yii::t('app', 'Date') ?></td>
        <td><?=Yii::t('app', 'Event')?></td>
        <td><?=Yii::t('app', 'Total events')?></td>
    </tr>

    <?php
   // echo '<pre>';
  //  print_r($calendar);
  //  echo '</pre>';
    ?>
<?php foreach ($calendar as $day => $events): ?>
        <tr>
            <td class="td-date"><span class="label label-success"><?= $day; ?></span></td>
            <td>
                <?php 
                if (count($events) > 0) {
                foreach ($events as $one_events){   
                echo Html::a($one_events->name, Url::to(['task/view', 'id' => $one_events->id]));
                echo '<br>'.$one_events->description .'</p><br>';
            
                }
                }else echo '-';
                ?>
            </td>
            <td class="td-event"><?= (count($events) > 0) ? Html::a(count($events), Url::to(['task/events', 'date' => $events[0]->due_date])) : '-';
                ?></td>
        </tr>
<?php endforeach; ?>
</table>