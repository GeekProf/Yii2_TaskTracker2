<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use \yiister\gentelella\widgets\Panel;

echo '<div class="row">';
echo '<div  class="col-xs-12 col-md-6 col-lg-4">';
echo Html::a(Yii::t('app', 'Create Group'), ['/group/create'], ['class' => 'btn btn-primary']);
echo '</div>';
echo '</div>';
echo '<div class="row">';

foreach ($items as $group):

    echo '<div  class="col-xs-12 col-md-6 col-lg-4">';

    Panel::begin(
            [
                'header' => $group->name,
                   'icon' => 'users'
            ]
    );

    echo '<p>' . $group->description . '</p>';

    echo Html::a(Yii::t('app', 'Group project'), ['project/group-project?groupid='.$group->id], ['class' => 'btn btn-success pull-right']);
    echo Html::a(Yii::t('app', 'Create Project'), ['/project/create?groupid='.$group->id], ['class' => 'btn btn-warning pull-right']);
    Panel::end();

    echo '</div>';


endforeach;
echo '</div>';
?>  
